package com.app.stt.main.model

/**
 * Created by Intellij Amiya
 * A good programmer is someone who looks both ways before crossing a One-way street.
 * Kindly follow https://source.android.com/setup/code-style
 */

import kotlin.collections.ArrayList


data class ModelData(val dictionary:ArrayList<Dictionary>)
data class Dictionary(val word:String, var frequency:Int)