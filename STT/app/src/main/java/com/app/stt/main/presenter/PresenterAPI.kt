package com.app.stt.main.presenter
/**
 * Created by Intellij Amiya
 * A good programmer is someone who looks both ways before crossing a One-way street.
 * Kindly follow https://source.android.com/setup/code-style
 */


import android.annotation.SuppressLint
import com.app.stt.api.BaseService
import com.app.stt.api.RetrofitHelper
import com.app.stt.main.inter.ICallBackData
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class PresenterAPI {

    @SuppressLint("CheckResult")
    fun getData(callBack: ICallBackData)
    {

        RetrofitHelper.getRetrofit(BaseService::class.java).getDictionary()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .map { responseBody ->
                responseBody
            }
            .subscribe({ response ->

                try
                {
                    if(response.dictionary.size>0)
                    {
                        callBack.onSuccess(response.dictionary)
                    }
                    else
                    {
                        callBack.onEmpty("No data found")
                    }

                }
                catch (e: Exception)
                {

                }
            }, {
                error ->

                  callBack.onError("Something went wrong")

            })
    }

}