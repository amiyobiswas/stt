package com.app.stt.common

/**
 * Created by Intellij Amiya
 * A good programmer is someone who looks both ways before crossing a One-way street.
 * Kindly follow https://source.android.com/setup/code-style
 */

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences

class SharedPref @SuppressLint("CommitPrefEdits")
constructor(context: Context) // Constructor
{

    private val pref: SharedPreferences
    private val editor: SharedPreferences.Editor

    var apiData: String
        get() = pref.getString(KEY_SET_API_DATA, "0")!!
        set(listData) {
            editor.remove(KEY_SET_API_DATA)
            editor.putString(KEY_SET_API_DATA, listData)
            editor.commit()
        }


    init {
        pref = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        editor = pref.edit()

    }

    companion object {
        private const val PREF_NAME = "STT"

        // All Shared Preferences Keys Declare as #public
        private const val KEY_SET_API_DATA    = "KEY_SET_API_DATA"

    }


}
