package com.app.stt.common

/**
 * Created by Intellij Amiya
 * A good programmer is someone who looks both ways before crossing a One-way street.
 * Kindly follow https://source.android.com/setup/code-style
 */

import android.content.Context
import android.graphics.Typeface
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.view.Gravity
import android.view.View
import android.widget.TextView
import com.google.android.material.snackbar.Snackbar

class CommonMethod {


    fun setSnackBar(context:Context, coordinatorLayout: View, snackTitle: String)
    {
        val snackbar = Snackbar.make(coordinatorLayout, snackTitle, Snackbar.LENGTH_SHORT)
        snackbar.show()
        val view = snackbar.view
        val txtv = view.findViewById(com.google.android.material.R.id.snackbar_text) as TextView
        val font = Typeface.createFromAsset(context.assets,"fonts/Montserrat-Regular.ttf")
        txtv.typeface = font
        txtv.gravity = Gravity.CENTER_HORIZONTAL
        txtv.textSize = 13F

    }

    //@IntRange(from = 0, to = 2)
    fun isNetworkAvailable(context: Context): Boolean {
        var result = false //var result = 0. Returns connection type. 0: none; 1: mobile data; 2: wifi
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            cm?.run {
                cm.getNetworkCapabilities(cm.activeNetwork)?.run {
                    if (hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                        result = true //2
                    } else if (hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                        result = true  //1
                    }
                }
            }
        } else {
            cm?.run {
                cm.activeNetworkInfo?.run {
                    if (type == ConnectivityManager.TYPE_WIFI) {
                        result = true //2
                    } else if (type == ConnectivityManager.TYPE_MOBILE) {
                        result = true //1
                    }
                }
            }
        }
        return result
    }
}