package com.app.stt.main.inter

/**
 * Created by Intellij Amiya
 * A good programmer is someone who looks both ways before crossing a One-way street.
 * Kindly follow https://source.android.com/setup/code-style
 */

import com.app.stt.main.model.Dictionary

interface ICallBackData {

    fun onSuccess(dictionary: ArrayList<Dictionary>)
    fun onEmpty(message: String)
    fun onError(message: String)
}