package com.app.stt.main.view

/**
 * Created by Intellij Amiya
 * A good programmer is someone who looks both ways before crossing a One-way street.
 * Kindly follow https://source.android.com/setup/code-style
 */


import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.speech.RecognizerIntent
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.stt.R
import com.app.stt.common.CommonMethod
import com.app.stt.common.SharedPref
import com.app.stt.main.adapter.AdapterData
import com.app.stt.main.inter.ICallBackData
import com.app.stt.main.model.Dictionary
import com.app.stt.main.presenter.PresenterAPI
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*
import java.util.*
import kotlin.collections.ArrayList



class MainActivity : AppCompatActivity(),ICallBackData {


    private var listDataOBJ: ArrayList<Dictionary>? = null
    private lateinit var adapterOBJ: AdapterData


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        toolbarFont(this@MainActivity)

        initUI()


    }

    private fun initUI()
    {
        listDataOBJ = ArrayList()

        buttonSpeech.setOnClickListener {

            val intent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)
            intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM)
            intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault())

            if (intent.resolveActivity(packageManager) != null) {
                startActivityForResult(intent, requestCode)
            } else {
                CommonMethod().setSnackBar(this@MainActivity,rootLayout,resources.getString(R.string.support_speech_not_found))
            }

        }

        val sharedOBJ = SharedPref(this@MainActivity)

        if(sharedOBJ.apiData=="0") // First Time
        {
            if(CommonMethod().isNetworkAvailable(this@MainActivity))
            {
                shimmerLayout.visibility= View.VISIBLE
                shimmerLayout.startShimmerAnimation()
                PresenterAPI().getData(this)
            }
            else
            {
                buttonRetry.visibility=View.VISIBLE
                CommonMethod().setSnackBar(this@MainActivity,rootLayout,resources.getString(R.string.please_connect_with_internet))
            }

        }
        else
        {
            // Fetch from Shared Pref.

            val turnsType = object : TypeToken<List<Dictionary>>() {}.type
            val getData = Gson().fromJson<List<Dictionary>>(sharedOBJ.apiData, turnsType)
            listDataOBJ?.addAll(getData)
            setData(-1)
        }

        buttonRetry.setOnClickListener {

            if(CommonMethod().isNetworkAvailable(this@MainActivity))
            {
                buttonRetry.visibility=View.INVISIBLE
                shimmerLayout.visibility= View.VISIBLE
                shimmerLayout.startShimmerAnimation()
                PresenterAPI().getData(this)
            }
            else
            {

                CommonMethod().setSnackBar(this@MainActivity,rootLayout,resources.getString(R.string.please_connect_with_internet))
            }
        }
    }

    override fun onSuccess(dictionary: ArrayList<Dictionary>) {

        shimmerLayout.stopShimmerAnimation()
        shimmerLayout.visibility= View.INVISIBLE
        val sortedList = dictionary.sortedWith(compareByDescending { it.frequency })
        val sharedPrefOBJ = SharedPref(this@MainActivity)
        val gsonObject = Gson().toJson(sortedList)
        sharedPrefOBJ.apiData = gsonObject

        listDataOBJ!!.addAll(sortedList)
        setData(-1)
    }

    override fun onEmpty(message: String) {

        CommonMethod().setSnackBar(this@MainActivity,rootLayout,message)
    }

    override fun onError(message: String) {

        CommonMethod().setSnackBar(this@MainActivity,rootLayout,message)
    }


    @SuppressLint("DefaultLocale")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            requestCode -> if (resultCode == Activity.RESULT_OK && data != null) {
                val result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS)
                val sharedPrefOBJ = SharedPref(this@MainActivity)

                var found=false
                var foundPosition=-1
                val locale = Locale.ENGLISH
                (0 until listDataOBJ!!.size).forEach {

                if(listDataOBJ!![it].word.toLowerCase(locale)==(result!![0]))
                    {


                        found=true
                        foundPosition=it


                    }

                }

                if(found)
                {
                    listDataOBJ!![foundPosition] = Dictionary(listDataOBJ!![foundPosition].word,listDataOBJ!![foundPosition].frequency+1)

                    val turnsType = object : TypeToken<List<Dictionary>>() {}.type
                    val getData = Gson().fromJson<List<Dictionary>>(sharedPrefOBJ.apiData, turnsType)
                    getData[foundPosition].frequency=getData[foundPosition].frequency+1
                    val json = Gson().toJson(getData)
                    sharedPrefOBJ.apiData = json

                    refreshData(result[0])
                }
                else
                {
                    setData(-1)
                    CommonMethod().setSnackBar(this@MainActivity,rootLayout,result[0]+ " "+resources.getString(R.string.not_present_in_list))

                }


            }
        }
    }

    private fun setData(position:Int)
    {
        adapterOBJ = AdapterData(this@MainActivity, listDataOBJ!!,position)
        rv.layoutManager = LinearLayoutManager(this@MainActivity, RecyclerView.VERTICAL, false)
        rv.adapter = adapterOBJ

    }


    private fun refreshData(speech: String)
    {
        val sharedPrefOBJ = SharedPref(this@MainActivity)
        val turnsType = object : TypeToken<List<Dictionary>>() {}.type
        val getData = Gson().fromJson<List<Dictionary>>(sharedPrefOBJ.apiData, turnsType)

        val sortedList = getData.sortedWith(compareByDescending { it.frequency })
        listDataOBJ?.clear()
        listDataOBJ?.addAll(sortedList)
        val gsonObject = Gson().toJson(listDataOBJ)
        sharedPrefOBJ.apiData = gsonObject

        var matchPosition=0
        val locale = Locale.ENGLISH

        (0 until listDataOBJ!!.size).forEach {

            if (listDataOBJ!![it].word.toLowerCase(locale).contentEquals(speech)) {
                matchPosition=it
            }
        }

       setData(matchPosition)
    }

    private fun toolbarFont(context: Activity) {

        for (i in 0 until toolbar.childCount) {
            val view = toolbar.getChildAt(i)
            if (view is TextView) {
                val titleFont = Typeface.createFromAsset(context.assets, "fonts/Montserrat-Regular.ttf")
                if (view.text == toolbar.title) {
                    view.typeface = titleFont
                    break
                }
            }
        }
    }


    companion object {

        private const val requestCode = 10


    }
}

