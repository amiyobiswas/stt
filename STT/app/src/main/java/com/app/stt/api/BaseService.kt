package com.app.stt.api

import com.app.stt.main.model.ModelData
import retrofit2.http.GET
import io.reactivex.Observable


interface BaseService{

    @GET("dictionary-v2.json")
    fun getDictionary(): Observable<ModelData>


}


