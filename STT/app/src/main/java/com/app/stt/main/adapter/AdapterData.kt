package com.app.stt.main.adapter

/**
 * Created by Intellij Amiya
 * A good programmer is someone who looks both ways before crossing a One-way street.
 * Kindly follow https://source.android.com/setup/code-style
 */

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.app.stt.R
import com.app.stt.main.model.Dictionary


class AdapterData (private val context: Context, private val listOBJ: ArrayList<Dictionary>,
                   matchPosition:Int) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

   private val positionOBJ=matchPosition

   /* override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int, payloads: MutableList<Any>) {

        val viewHolder = holder as SelectViewHolder
        if (payloads.isNotEmpty()) {

            viewHolder.rootLayoutOBJ.setBackgroundColor(Color.YELLOW)

        }


        super.onBindViewHolder(holder, position, payloads)
    }*/

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        val viewHolder = holder as SelectViewHolder
        viewHolder.txtWordOBJ.text=listOBJ[position].word
        viewHolder.txtFrequencyOBJ.text=listOBJ[position].frequency.toString()
        if(positionOBJ==position)
        {
            viewHolder.rootLayoutOBJ.setBackgroundColor(ContextCompat.getColor(context,R.color.colorLightYellow))
        }
    }

    override fun getItemCount(): Int = listOBJ.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        val recyclerView: RecyclerView.ViewHolder

        val itemView: View = LayoutInflater.from(parent.context).inflate(R.layout.layout_list, parent, false)
        recyclerView = SelectViewHolder(itemView)
        return recyclerView
    }

    companion object {
        private class SelectViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

            var txtWordOBJ       : TextView         = itemView.findViewById(R.id.txtWord) as TextView
            var txtFrequencyOBJ  : TextView         = itemView.findViewById(R.id.txtFrequency) as TextView
            var rootLayoutOBJ    : ConstraintLayout = itemView.findViewById(R.id.rootLayout) as ConstraintLayout

        }

    }
}