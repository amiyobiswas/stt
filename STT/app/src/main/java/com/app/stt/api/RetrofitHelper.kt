package com.app.stt.api


import com.app.stt.BuildConfig
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


class RetrofitHelper {

    companion object {

        private const val APPLICATION_JSON = "application/json"
        private const val CLIENT_TYPE = "A"

        private fun getOkHttpClient(accessToken: String): OkHttpClient {
            val okHttpClient = OkHttpClient.Builder()
            okHttpClient.readTimeout(120, TimeUnit.SECONDS)
            okHttpClient.connectTimeout(120, TimeUnit.SECONDS)

            okHttpClient.addInterceptor { chain ->
                val original = chain.request()

                val requestBuilder = original.newBuilder()
                        .header("Accept", APPLICATION_JSON)
                        .header("Content-Type", APPLICATION_JSON)
                        .header("Client-Type", CLIENT_TYPE)
                        .header("Build", BuildConfig.VERSION_CODE.toString())
                        .header("Version", BuildConfig.VERSION_NAME)

                if (accessToken.isNotEmpty()) {
                    requestBuilder.header("Authorization", accessToken)
                }

                val request = requestBuilder.build()

                chain.proceed(request)
            }

            return okHttpClient.build()
        }
        fun <T> getRetrofit(service: Class<T>): T {
            val retrofit = Retrofit.Builder()
                .baseUrl("http://a.galactio.com/interview/")
                .client(getOkHttpClient(""))
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
            return retrofit.create(service)
        }

    }


}


